import 'package:test/test.dart';

import "package:flutter_package/problem1/problem1.dart";

void main() {
  test("case 0", () {
    expect(solvePart2([1, -1]), equals(0));
  });

  test("case 1", () {
    expect(solvePart2([3, 3, 4, -2, -4]), equals(10));
  });

  test("case 2", () {
    expect(solvePart2([-6, 3, 8, 5, -6]), equals(5));
  });

  test("case 3", () {
    expect(solvePart2([7, 7, -2, -7, -4]), equals(14));
  });
}
