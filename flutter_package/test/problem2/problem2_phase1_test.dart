import 'package:test/test.dart';

import 'package:flutter_package/problem2/problem2.dart';

void main() {
  test('case 1', () {
    List<String> ids = [
      'abcdef',
      'bababc',
      'abbcde',
      'abcccd',
      'aabcdd',
      'abcdee',
      'ababab',
    ];
    expect(solvePhase1(ids), equals(12));
  });
}
