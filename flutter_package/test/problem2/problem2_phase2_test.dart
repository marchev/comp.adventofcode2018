import 'package:test/test.dart';

import 'package:flutter_package/problem2/problem2.dart';

void main() {
  test('case 1', () {
    List<String> ids = [
      'abcde',
      'fghij',
      'klmno',
      'pqrst',
      'fguij',
      'axcye',
      'wvxyz',
    ];

    expect(solvePhase2(ids), equals('fgij'));
  });
}
