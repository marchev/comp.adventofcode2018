import 'package:test/test.dart';
import 'package:flutter_package/problem14/problem14.dart';

void main() {
  group('phase 1', () {
    test('with given case 1', () {
      expect(solvePhase1(9), equals('5158916779'));
    });

    test('with given case 2', () {
      expect(solvePhase1(5), equals('0124515891'));
    });

    test('with given case 3', () {
      expect(solvePhase1(18), equals('9251071085'));
    });

    test('with given case 4', () {
      expect(solvePhase1(2018), equals('5941429882'));
    });

    test('with file input', () {
      expect(solvePhase1(), equals('4138145721'));
    });
  });

  group('phase 2', () {
    test('with given case 1', () {
      expect(solvePhase2('51589'), equals(9));
    });

    test('with given case 2', () {
      expect(solvePhase2('01245'), equals(5));
    });

    test('with given case 3', () {
      expect(solvePhase2('92510'), equals(18));
    });

    test('with given case 4', () {
      expect(solvePhase2('59414'), equals(2018));
    });

    test('with file input', () {
      expect(solvePhase2(), equals(20276284));
    });
  });
}
