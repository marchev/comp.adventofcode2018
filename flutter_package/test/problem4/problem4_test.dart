import 'package:test/test.dart';
import 'package:flutter_package/problem4/problem4.dart';

void main() {
  group('phase 1', () {
    test('given case 1', () {
      List<Event> events = [
        Event(DateTime.parse('1518-11-01 00:00'), EventType.startsShift, 10),
        Event(DateTime.parse('1518-11-01 00:05'), EventType.fallsAsleep, null),
        Event(DateTime.parse('1518-11-01 00:25'), EventType.wakesUp, null),
        Event(DateTime.parse('1518-11-01 00:30'), EventType.fallsAsleep, null),
        Event(DateTime.parse('1518-11-01 00:55'), EventType.wakesUp, null),
        Event(DateTime.parse('1518-11-01 23:58'), EventType.startsShift, 99),
        Event(DateTime.parse('1518-11-02 00:40'), EventType.fallsAsleep, null),
        Event(DateTime.parse('1518-11-02 00:50'), EventType.wakesUp, null),
        Event(DateTime.parse('1518-11-03 00:05'), EventType.startsShift, 10),
        Event(DateTime.parse('1518-11-03 00:24'), EventType.fallsAsleep, null),
        Event(DateTime.parse('1518-11-03 00:29'), EventType.wakesUp, null),
        Event(DateTime.parse('1518-11-04 00:02'), EventType.startsShift, 99),
        Event(DateTime.parse('1518-11-04 00:36'), EventType.fallsAsleep, null),
        Event(DateTime.parse('1518-11-04 00:46'), EventType.wakesUp, null),
        Event(DateTime.parse('1518-11-05 00:03'), EventType.startsShift, 99),
        Event(DateTime.parse('1518-11-05 00:45'), EventType.fallsAsleep, null),
        Event(DateTime.parse('1518-11-05 00:55'), EventType.wakesUp, null),
      ];
      expect(solvePhase1(events), equals(10 * 24));
    });

    test('with data from file', () {
      List<Event> events = readInput();
      // Test created after verifying the answer is correct.
      expect(solvePhase1(events), equals(38813));
    });
  });

  group('phase 2', () {
    test('given case 1', () {
      List<Event> events = [
        Event(DateTime.parse('1518-11-01 00:00'), EventType.startsShift, 10),
        Event(DateTime.parse('1518-11-01 00:05'), EventType.fallsAsleep, null),
        Event(DateTime.parse('1518-11-01 00:25'), EventType.wakesUp, null),
        Event(DateTime.parse('1518-11-01 00:30'), EventType.fallsAsleep, null),
        Event(DateTime.parse('1518-11-01 00:55'), EventType.wakesUp, null),
        Event(DateTime.parse('1518-11-01 23:58'), EventType.startsShift, 99),
        Event(DateTime.parse('1518-11-02 00:40'), EventType.fallsAsleep, null),
        Event(DateTime.parse('1518-11-02 00:50'), EventType.wakesUp, null),
        Event(DateTime.parse('1518-11-03 00:05'), EventType.startsShift, 10),
        Event(DateTime.parse('1518-11-03 00:24'), EventType.fallsAsleep, null),
        Event(DateTime.parse('1518-11-03 00:29'), EventType.wakesUp, null),
        Event(DateTime.parse('1518-11-04 00:02'), EventType.startsShift, 99),
        Event(DateTime.parse('1518-11-04 00:36'), EventType.fallsAsleep, null),
        Event(DateTime.parse('1518-11-04 00:46'), EventType.wakesUp, null),
        Event(DateTime.parse('1518-11-05 00:03'), EventType.startsShift, 99),
        Event(DateTime.parse('1518-11-05 00:45'), EventType.fallsAsleep, null),
        Event(DateTime.parse('1518-11-05 00:55'), EventType.wakesUp, null),
      ];
      expect(solvePhase2(events), equals(99 * 45));
    });

    test('with data from file', () {
      List<Event> events = readInput();
      // Test created after verifying the answer is correct.
      expect(solvePhase2(events), equals(141071));
    });
  });
}
