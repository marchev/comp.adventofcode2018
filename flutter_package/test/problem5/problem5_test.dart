import 'package:test/test.dart';
import 'package:flutter_package/problem5/problem5.dart';

void main() {
  group('phase 1', () {
    test('two reacting', () {
      String s = 'cC';
      expect(solvePhase1(s), equals(0));
    });

    test('one char', () {
      String s = 'C';
      expect(solvePhase1(s), equals(1));
    });

    test('reaction in a chain', () {
      String s = 'OabBAo';
      expect(solvePhase1(s), equals(0));
    });

    test('given case 1', () {
      String s = 'dabAcCaCBAcCcaDA';
      expect(solvePhase1(s), equals(10));
    });

    test('with file input', () {
      String s = readInput();
      expect(solvePhase1(s), equals(10638));
    });
  });

  group('phase 2', () {
    test('given case 1', () {
      String s = 'dabAcCaCBAcCcaDA';
      expect(solvePhase2(s), equals(4));
    });

    test('with file input', () {
      String s = readInput();
      expect(solvePhase2(s), equals(4944));
    });
  });
}
