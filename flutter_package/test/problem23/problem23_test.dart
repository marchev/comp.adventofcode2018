import 'package:test/test.dart';
import 'package:flutter_package/problem23/problem23.dart';

void main() {
  group('phase 1', () {
    test('with given case 1', () {
      List<String> lines = [
        'pos=<0,0,0>, r=4',
        'pos=<1,0,0>, r=1',
        'pos=<4,0,0>, r=3',
        'pos=<0,2,0>, r=1',
        'pos=<0,5,0>, r=3',
        'pos=<0,0,3>, r=1',
        'pos=<1,1,1>, r=1',
        'pos=<1,1,2>, r=1',
        'pos=<1,3,1>, r=1',
      ];
      expect(solvePhase1(convertToBots(lines)), equals(7));
    });

    test('with file input', () {
      expect(solvePhase1(convertToBots(readLines())), equals(935));
    });
  });
}
