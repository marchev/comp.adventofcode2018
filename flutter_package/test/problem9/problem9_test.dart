import 'package:test/test.dart';
import 'package:flutter_package/problem9/problem9.dart';

void main() {
  group('phase 1', () {
    test('with 9 players', () {
      Config config = Config(9, 25);
      expect(solvePhase1(config), equals(32));
    });

    test('with 10 players', () {
      Config config = Config(10, 1618);
      expect(solvePhase1(config), equals(8317));
    });

    test('with 13 players', () {
      Config config = Config(13, 7999);
      expect(solvePhase1(config), equals(146373));
    });

    test('with 17 players', () {
      Config config = Config(17, 1104);
      expect(solvePhase1(config), equals(2764));
    });

    test('with 21 players', () {
      Config config = Config(21, 6111);
      expect(solvePhase1(config), equals(54718));
    });

    test('with 30 players', () {
      Config config = Config(30, 5807);
      expect(solvePhase1(config), equals(37305));
    });

    test('with file input', () {
      expect(solvePhase1(), equals(404502));
    });
  });

  group('phase 2', () {
    test('with file input', () {
      expect(solvePhase2(), equals(3243916887));
    });
  });
}
