import 'package:test/test.dart';
import 'package:flutter_package/problem8/problem8.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      List<int> serialization = [
        2,
        3,
        0,
        3,
        10,
        11,
        12,
        1,
        1,
        0,
        1,
        99,
        2,
        1,
        1,
        2
      ];
      expect(solvePhase1(serialization), equals(138));
    });

    test('with file input', () {
      List<int> serialization = readTreeSerialization();
      expect(solvePhase1(serialization), equals(43825));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      List<int> serialization = [
        2,
        3,
        0,
        3,
        10,
        11,
        12,
        1,
        1,
        0,
        1,
        99,
        2,
        1,
        1,
        2
      ];
      expect(solvePhase2(serialization), equals(66));
    });

    test('with file input', () {
      List<int> serialization = readTreeSerialization();
      expect(solvePhase2(serialization), equals(19276));
    });
  });
}
