import 'package:test/test.dart';
import 'package:flutter_package/problem10/problem10.dart';

List<Beam> transpose(List<Beam> beams) {
  return beams
      .map((b) => Beam(Point(b.initialPos.c, b.initialPos.r), b.dc, b.dr))
      .toList();
}

void main() {
  group('phase 1', () {
    test('with given case', () {
      List<Beam> beams = transpose([
        Beam(Point(9, 1), 0, 2),
        Beam(Point(7, 0), -1, 0),
        Beam(Point(3, -2), -1, 1),
        Beam(Point(6, 10), -2, -1),
        Beam(Point(2, -4), 2, 2),
        Beam(Point(-6, 10), 2, -2),
        Beam(Point(1, 8), 1, -1),
        Beam(Point(1, 7), 1, 0),
        Beam(Point(-3, 11), 1, -2),
        Beam(Point(7, 6), -1, -1),
        Beam(Point(-2, 3), 1, 0),
        Beam(Point(-4, 3), 2, 0),
        Beam(Point(10, -3), -1, 1),
        Beam(Point(5, 11), 1, -2),
        Beam(Point(4, 7), 0, -1),
        Beam(Point(8, -2), 0, 1),
        Beam(Point(15, 0), -2, 0),
        Beam(Point(1, 6), 1, 0),
        Beam(Point(8, 9), 0, -1),
        Beam(Point(3, 3), -1, 1),
        Beam(Point(0, 5), 0, -1),
        Beam(Point(-2, 2), 2, 0),
        Beam(Point(5, -2), 1, 2),
        Beam(Point(1, 4), 2, 1),
        Beam(Point(-2, 7), 2, -2),
        Beam(Point(3, 6), -1, -1),
        Beam(Point(5, 0), 1, 0),
        Beam(Point(-6, 0), 2, 0),
        Beam(Point(5, 9), 1, -2),
        Beam(Point(14, 7), -2, 0),
        Beam(Point(-3, 6), 2, -1),
      ]);
      solvePhase1(beams);
    });

    test('with file input', () {
      List<Beam> beams = readBeams();
      solvePhase1(beams);
    });
  });

  group('phase 2', () {
    test('with given case', () {
      List<Beam> beams = transpose([
        Beam(Point(9, 1), 0, 2),
        Beam(Point(7, 0), -1, 0),
        Beam(Point(3, -2), -1, 1),
        Beam(Point(6, 10), -2, -1),
        Beam(Point(2, -4), 2, 2),
        Beam(Point(-6, 10), 2, -2),
        Beam(Point(1, 8), 1, -1),
        Beam(Point(1, 7), 1, 0),
        Beam(Point(-3, 11), 1, -2),
        Beam(Point(7, 6), -1, -1),
        Beam(Point(-2, 3), 1, 0),
        Beam(Point(-4, 3), 2, 0),
        Beam(Point(10, -3), -1, 1),
        Beam(Point(5, 11), 1, -2),
        Beam(Point(4, 7), 0, -1),
        Beam(Point(8, -2), 0, 1),
        Beam(Point(15, 0), -2, 0),
        Beam(Point(1, 6), 1, 0),
        Beam(Point(8, 9), 0, -1),
        Beam(Point(3, 3), -1, 1),
        Beam(Point(0, 5), 0, -1),
        Beam(Point(-2, 2), 2, 0),
        Beam(Point(5, -2), 1, 2),
        Beam(Point(1, 4), 2, 1),
        Beam(Point(-2, 7), 2, -2),
        Beam(Point(3, 6), -1, -1),
        Beam(Point(5, 0), 1, 0),
        Beam(Point(-6, 0), 2, 0),
        Beam(Point(5, 9), 1, -2),
        Beam(Point(14, 7), -2, 0),
        Beam(Point(-3, 6), 2, -1),
      ]);
      expect(solvePhase2(beams), equals(3));
    });

    test('with file input', () {
      List<Beam> beams = readBeams();
      expect(solvePhase2(beams), equals(10612));
    });
  });
}
