import 'package:test/test.dart';
import 'package:flutter_package/problem25/problem25.dart';

void main() {
  group('phase 1', () {
    test('with given case 1', () {
      List<String> lines = [
        '0,0,0,0',
        '3,0,0,0',
        '0,3,0,0',
        '0,0,3,0',
        '0,0,0,3',
        '0,0,0,6',
        '9,0,0,0',
        '12,0,0,0',
      ];
      expect(solvePhase1(convertToPoints(lines)), equals(2));
    });

    test('with given case 2', () {
      List<Point> points = [
        Point([-1, 2, 2, 0]),
        Point([0, 0, 2, -2]),
        Point([0, 0, 0, -2]),
        Point([-1, 2, 0, 0]),
        Point([-2, -2, -2, 2]),
        Point([3, 0, 2, -1]),
        Point([-1, 3, 2, 2]),
        Point([-1, 0, -1, 0]),
        Point([0, 2, 1, -2]),
        Point([3, 0, 0, 0]),
      ];
      expect(solvePhase1(points), equals(4));
    });

    test('with given case 3', () {
      List<Point> points = [
        Point([1, -1, 0, 1]),
        Point([2, 0, -1, 0]),
        Point([3, 2, -1, 0]),
        Point([0, 0, 3, 1]),
        Point([0, 0, -1, -1]),
        Point([2, 3, -2, 0]),
        Point([-2, 2, 0, 0]),
        Point([2, -2, 0, -1]),
        Point([1, -1, 0, -1]),
        Point([3, 2, 0, 2]),
      ];
      expect(solvePhase1(points), equals(3));
    });

    test('with given case 4', () {
      List<Point> points = [
        Point([1, -1, -1, -2]),
        Point([-2, -2, 0, 1]),
        Point([0, 2, 1, 3]),
        Point([-2, 3, -2, 1]),
        Point([0, 2, 3, -2]),
        Point([-1, -1, 1, -2]),
        Point([0, -2, -1, 0]),
        Point([-2, 2, 3, -1]),
        Point([1, 2, 2, 0]),
        Point([-1, -2, 0, -2]),
      ];
      expect(solvePhase1(points), equals(8));
    });

    test('with file input', () {
      expect(solvePhase1(convertToPoints(readLines())), equals(420));
    });
  });
}
