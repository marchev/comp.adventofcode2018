import 'package:test/test.dart';
import 'package:flutter_package/problem11/problem11.dart';

void main() {
  test('computeCellPower', () {
    expect(computeCellPower(3, 5, 8), equals(4));
    expect(computeCellPower(122, 79, 57), equals(-5));
    expect(computeCellPower(217, 196, 39), equals(0));
    expect(computeCellPower(101, 153, 71), equals(4));
  });

  group('phase 1', () {
    test('with given case', () {
      expect(solvePhase1(18), equals('33,45'));
    });

    test('with file input', () {
      expect(solvePhase1(), equals('235,35'));
    });
  });

  group('phase 2', () {
    test('with given case 1', () {
      expect(solvePhase2(18), equals('90,269,16'));
    });

    test('with given case 2', () {
      expect(solvePhase2(42), equals('232,251,12'));
    });

    test('with file input', () {
      expect(solvePhase2(), equals('142,265,7'));
    });
  });
}
