import 'package:test/test.dart';
import 'package:flutter_package/problem17/problem17.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      List<ClaySegment> segments = [
        ClaySegment(495, 495, 2, 7),
        ClaySegment(495, 501, 7, 7),
        ClaySegment(501, 501, 3, 7),
        ClaySegment(498, 498, 2, 4),
        ClaySegment(506, 506, 1, 2),
        ClaySegment(498, 498, 10, 13),
        ClaySegment(498, 504, 13, 13),
        ClaySegment(504, 504, 10, 13),
      ];
      expect(solvePhase1(segments), equals(57));
    });

    test('with file input', () {
      expect(solvePhase1(readSegments()), equals(31953));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      List<ClaySegment> segments = [
        ClaySegment(495, 495, 2, 7),
        ClaySegment(495, 501, 7, 7),
        ClaySegment(501, 501, 3, 7),
        ClaySegment(498, 498, 2, 4),
        ClaySegment(506, 506, 1, 2),
        ClaySegment(498, 498, 10, 13),
        ClaySegment(498, 504, 13, 13),
        ClaySegment(504, 504, 10, 13),
      ];
      expect(solvePhase2(segments), equals(29));
    });

    test('with file input', () {
      expect(solvePhase2(readSegments()), equals(26410));
    });
  });
}
