import 'package:test/test.dart';
import 'package:flutter_package/problem13/problem13.dart';

void main() {
  group('phase 1', () {
    test('with given case 1', () {
      List<String> lines = [
        r'/->-\        ',
        r'|   |  /----\',
        r'| /-+--+-\  |',
        r'| | |  | v  |',
        r'\-+-/  \-+--/',
        r'  \------/   ',
      ];
      expect(solvePhase1(lines), equals('7,3'));
    });

    test('with file input', () {
      expect(solvePhase1(readLines()), equals('80,100'));
    });
  });

  group('phase 2', () {
    test('with given case 1', () {
      List<String> lines = [
        r'/>-<\  ',
        r'|   |  ',
        r'| /<+-\',
        r'| | | v',
        r'\>+</ |',
        r'  |   ^',
        r'  \<->/',
      ];
      expect(solvePhase2(lines), equals('6,4'));
    });

    test('with file input', () {
      expect(solvePhase2(readLines()), equals('16,99'));
    });
  });
}
