import 'package:test/test.dart';
import 'package:flutter_package/problem12/problem12.dart';

void main() {
  group('phase 1', () {
    test('with given case 1', () {
      Config conf = Config('#..#.#..##......###...###', {
        '...##': '#',
        '..#..': '#',
        '.#...': '#',
        '.#.#.': '#',
        '.#.##': '#',
        '.##..': '#',
        '.####': '#',
        '#.#.#': '#',
        '#.###': '#',
        '##.#.': '#',
        '##.##': '#',
        '###..': '#',
        '###.#': '#',
        '####.': '#',
      });
      expect(solvePhase1(conf), equals(325));
    });

    test('with file input', () {
      expect(solvePhase1(readConfig()), equals(3915));
    });
  });

  group('phase 2', () {
    test('with file input', () {
      expect(solvePhase2(readConfig()), equals(4900000001793));
    });
  });
}
