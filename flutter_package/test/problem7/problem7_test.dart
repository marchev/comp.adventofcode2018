import 'package:test/test.dart';
import 'package:flutter_package/problem7/problem7.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      List<Edge> edges = [
        Edge('C', 'A'),
        Edge('C', 'F'),
        Edge('A', 'B'),
        Edge('A', 'D'),
        Edge('B', 'E'),
        Edge('D', 'E'),
        Edge('F', 'E'),
      ];
      expect(solvePhase1(edges), equals('CABDFE'));
    });

    test('with file input', () {
      List<Edge> points = readEdges();
      expect(solvePhase1(points), equals('BFKEGNOVATIHXYZRMCJDLSUPWQ'));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      List<Edge> edges = [
        Edge('C', 'A'),
        Edge('C', 'F'),
        Edge('A', 'B'),
        Edge('A', 'D'),
        Edge('B', 'E'),
        Edge('D', 'E'),
        Edge('F', 'E'),
      ];
      expect(
          solvePhase2(edges,
              numWorkers: 2,
              getTimeForTask: (node) =>
                  node.codeUnitAt(0) - 'A'.codeUnitAt(0) + 1),
          equals(15));
    });

    test('with file input', () {
      List<Edge> points = readEdges();
      expect(solvePhase2(points), equals(1020));
    });
  });
}
