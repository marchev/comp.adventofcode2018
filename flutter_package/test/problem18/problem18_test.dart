import 'package:test/test.dart';
import 'package:flutter_package/problem18/problem18.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      List<List<String>> grid = createGrid([
        '.#.#...|#.',
        '.....#|##|',
        '.|..|...#.',
        '..|#.....#',
        '#.#|||#|#|',
        '...#.||...',
        '.|....|...',
        '||...#|.#|',
        '|.||||..|.',
        '...#.|..|.',
      ]);
      expect(solvePhase1(grid), equals(1147));
    });

    test('with file input', () {
      expect(solvePhase1(createGrid(readRows())), equals(456225));
    });
  });

  group('phase 2', () {
    test('with file input', () {
      expect(solvePhase2(createGrid(readRows())), equals(190164));
    });
  });
}
