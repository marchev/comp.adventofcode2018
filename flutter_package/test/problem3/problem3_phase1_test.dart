import 'package:test/test.dart';
import 'package:flutter_package/problem3/problem3.dart';

void main() {
  test('case 1', () {
    List<Rect> rects = [
      Rect(1, 1, 3, 4, 4),
      Rect(2, 3, 1, 4, 4),
      Rect(3, 5, 5, 2, 2),
    ];
    expect(solvePhase1(rects), equals(4));
  });
}
