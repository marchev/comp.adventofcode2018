import 'package:test/test.dart';
import 'package:flutter_package/problem6/problem6.dart';

void main() {
  group('phase 1', () {
    test('with given case', () {
      List<Point> points = [
        Point(1, 1),
        Point(1, 6),
        Point(8, 3),
        Point(3, 4),
        Point(5, 5),
        Point(8, 9),
      ];
      expect(solvePhase1(points), equals(17));
    });

    test('with file input', () {
      List<Point> points = readPoints();
      expect(solvePhase1(points), equals(3890));
    });
  });

  group('phase 2', () {
    test('with given case', () {
      List<Point> points = [
        Point(1, 1),
        Point(1, 6),
        Point(8, 3),
        Point(3, 4),
        Point(5, 5),
        Point(8, 9),
      ];
      expect(solvePhase2(points, maxDist: 32), equals(16));
    });

    test('with file input', () {
      List<Point> points = readPoints();
      expect(solvePhase2(points), equals(40284));
    });
  });
}
