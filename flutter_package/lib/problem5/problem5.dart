/// https://adventofcode.com/2018, Day 5
///
/// Task: https://adventofcode.com/2018/day/5
/// Date: 2018-12-24
/// Author: Svilen Marchev
///
/// Status: works for both phases, 1.8s for phase 1, and ~40s for phase 2
///
/// Phase 1 answer: 10638
/// Phase 2 answer: 4944

import 'dart:collection';
import 'dart:io';

String readInput() {
  return File('lib/problem5/input.txt').readAsStringSync().trimRight();
}

class Node extends LinkedListEntry<Node> {
  /// The original case-sensitive char. Used for presentation.
  String rawChar;

  /// Code of the normalized char (always lower case).
  int charCode;

  /// True if the char has been lower case, false otherwise.
  bool isPositive;

  Node(String s) {
    assert(s.length == 1);
    rawChar = s;
    isPositive = (s.toLowerCase() == s);
    charCode = s.toLowerCase().codeUnitAt(0);
  }

  bool doesReactWith(Node n) {
    if (n == null) {
      return false;
    }
    return charCode == n.charCode && (isPositive ^ n.isPositive);
  }

  @override
  toString() => rawChar;
}

num solvePhase1(String s) {
  LinkedList<Node> list = LinkedList();
  for (int i = 0; i < s.length; ++i) {
    list.add(Node(s[i]));
  }

  bool hasErasedInCurLoop = true;
  while (hasErasedInCurLoop && list.length > 1) {
    hasErasedInCurLoop = false;
    for (Node a = list.first; a != null; a = a.next) {
      if (a.doesReactWith(a.previous)) {
        hasErasedInCurLoop = true;
        a.previous.unlink();
        a.unlink();
      }
    }
  }

  return list.length;
}

num solvePhase2(String s) {
  var minReduction = s.length;
  for (int c = 'a'.codeUnitAt(0); c <= 'z'.codeUnitAt(0); ++c) {
    var replaceLower = String.fromCharCode(c);
    var replaceUpper = replaceLower.toUpperCase();
    var replaced = s.replaceAll(RegExp('[$replaceLower$replaceUpper]'), '');
    print(
        'Len to reduce after replacing $replaceLower/$replaceUpper: ${replaced.length}');

    var reducedLength = solvePhase1(replaced);
    print('Will be reduced to: $reducedLength');
    if (minReduction > reducedLength) {
      minReduction = reducedLength;
    }
  }
  return minReduction;
}

void main() {
  String s = readInput();

  print('Phase 1: Ans = ${solvePhase1(s)}');

  Stopwatch stopwatch = new Stopwatch()..start();
  print('Phase 2: Ans = ${solvePhase2(s)}');
  print('time = ${stopwatch.elapsed}');
}
