import 'dart:io';

class Rect {
  int id;
  int left, top, w, h;

  Rect(this.id, this.left, this.top, this.w, this.h);

  @override
  String toString() =>
      "${this.id}: ${this.left},${this.top} ${this.w}x${this.h}";
}

List<Rect> readInput() {
  // Parse this format:
  // #5 @ 553,529: 25x11
  RegExp exp = new RegExp(r'^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$');
  return File('lib/problem3/input.txt').readAsLinesSync().map((line) {
    Match m = exp.firstMatch(line);
    Rect r = Rect(
      int.parse(m.group(1)),
      int.parse(m.group(2)),
      int.parse(m.group(3)),
      int.parse(m.group(4)),
      int.parse(m.group(5)),
    );
    print("$r");
    return r;
  }).toList();
}

List<List<bool>> computeOverlapMatrix(List<Rect> rects) {
  List<List<bool>> a = List.generate(1000, (_) => List(1000));
  for (Rect rect in rects) {
    for (int r = 0; r < rect.h; ++r) {
      for (int c = 0; c < rect.w; ++c) {
        int rr = rect.top + r;
        int rc = rect.left + c;

        if (a[rr][rc] == null) {
          a[rr][rc] = false;
        } else if (!a[rr][rc]) {
          a[rr][rc] = true;
        }
      }
    }
  }
  return a;
}

int solvePhase1(List<Rect> rects) {
  List<List<bool>> a = computeOverlapMatrix(rects);
  int overlap = 0;
  for (int r = 0; r < a.length; ++r) {
    for (int c = 0; c < a[r].length; ++c) {
      overlap += a[r][c] != null && a[r][c] ? 1 : 0;
    }
  }
  return overlap;
}

int solvePhase2(List<Rect> rects) {
  List<List<bool>> a = computeOverlapMatrix(rects);
  rects_loop:
  for (Rect rect in rects) {
    for (int r = 0; r < rect.h; ++r) {
      for (int c = 0; c < rect.w; ++c) {
        int rr = rect.top + r;
        int rc = rect.left + c;
        if (a[rr][rc]) {
          continue rects_loop;
        }
      }
    }
    return rect.id;
  }
  throw AssertionError();
}

void main() {
  List<Rect> rects = readInput();

  print("Phase 1: Ans = ${solvePhase1(rects)}");
  print("Phase 2: Ans = ${solvePhase2(rects)}");
}
