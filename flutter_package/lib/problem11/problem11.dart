/// https://adventofcode.com/2018, Day 11
///
/// Task: https://adventofcode.com/2018/day/11
/// Date: 2018-12-25
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: 235,35, for 0.01s
/// Phase 2 answer: 142,265,7, for 0.04s

class Result {
  int sum;
  int x, y;
  int size;

  Result(this.x, this.y, this.size, this.sum);
}

const int gridSize = 300;

int computeCellPower(int x, int y, int serialNum) =>
    ((x + 10) * y + serialNum) * (x + 10) ~/ 100 % 10 - 5;

List<List<int>> generateGrid(int serialNum) {
  return List.generate(gridSize,
      (y) => List.generate(gridSize, (x) => computeCellPower(x, y, serialNum)));
}

Result compute(List<List<int>> accum, int size) {
  int maxSum;
  int maxX, maxY;
  for (int y = size; y <= gridSize; ++y) {
    for (int x = size; x <= gridSize; ++x) {
      int sum = accum[y][x] -
          accum[y - size][x] -
          accum[y][x - size] +
          accum[y - size][x - size];
      if (maxSum == null || maxSum < sum) {
        maxSum = sum;
        maxX = x - size;
        maxY = y - size;
      }
    }
  }
  return Result(maxX, maxY, size, maxSum);
}

List<List<int>> createAccum(int serialNum) {
  List<List<int>> grid = generateGrid(serialNum);
  List<List<int>> accum =
      List.generate(gridSize + 1, (_) => List.filled(gridSize + 1, 0));
  for (int y = 1; y <= gridSize; ++y) {
    for (int x = 1; x <= gridSize; ++x) {
      accum[y][x] = accum[y - 1][x] +
          accum[y][x - 1] -
          accum[y - 1][x - 1] +
          grid[y - 1][x - 1];
    }
  }
  return accum;
}

String solvePhase1([int serialNum = 1788]) {
  List<List<int>> accum = createAccum(serialNum);
  Result result = compute(accum, 3);
  return '${result.x},${result.y}';
}

String solvePhase2([int serialNum = 1788]) {
  List<List<int>> accum = createAccum(serialNum);
  Result maxResult = compute(accum, 1);
  for (int size = 2; size <= gridSize; ++size) {
    Result result = compute(accum, size);
    if (maxResult.sum < result.sum) {
      maxResult = result;
    }
  }
  return '${maxResult.x},${maxResult.y},${maxResult.size}';
}

void main() {
  print('Phase 1: Ans = ${solvePhase1()}');
  print('Phase 2: Ans = ${solvePhase2()}');
}
