/// https://adventofcode.com/2018, Day 9
///
/// Task: https://adventofcode.com/2018/day/9
/// Date: 2018-12-24
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: 404502, for 0.06s
/// Phase 2 answer: 3243916887, for 1.8s

import 'dart:collection';
import 'package:quiver/iterables.dart';

class Config {
  final int numPlayers;
  final int lastMarble;

  const Config(this.numPlayers, this.lastMarble);
}

class Node extends LinkedListEntry<Node> {
  int value;

  Node(this.value);

  @override
  String toString() => '[$value]';
}

class Game {
  Config config;

  LinkedList<Node> list;
  Node curNode;
  int nextPlayer; // 0-based
  int nextMarble; // 1-based

  Map<int, int> playerToScore;

  Game(this.config) {
    list = LinkedList();
    list.add(Node(0));
    curNode = list.first;
    nextPlayer = 0;
    nextMarble = 1;
    playerToScore = Map();
  }

  bool makeMove() {
    if (nextMarble > config.lastMarble) {
      return false;
    }

    if (nextMarble % 23 != 0) {
      _moveRight(1);
      curNode.insertAfter(Node(nextMarble));
      _moveRight(1);
    } else {
      playerToScore.update(nextPlayer, (val) => val + nextMarble,
          ifAbsent: () => nextMarble);
      _moveLeft(7);
      playerToScore[nextPlayer] += curNode.value;
      Node toDelete = curNode;
      _moveRight(1);
      toDelete.unlink();
    }

    nextMarble++;
    nextPlayer = (nextPlayer + 1) % config.numPlayers;
    return true;
  }

  int getHighScore() {
    return max(playerToScore.values);
  }

  void _moveRight(int n) {
    n %= list.length;
    for (int i = 0; i < n; ++i) {
      curNode = curNode.next;
      if (curNode == null) {
        curNode = list.first;
      }
    }
  }

  void _moveLeft(int n) {
    n %= list.length;
    for (int i = 0; i < n; ++i) {
      curNode = curNode.previous;
      if (curNode == null) {
        curNode = list.last;
      }
    }
  }
}

/// Taken from the input.txt file.
const Config phase1Config = Config(458, 72019);
const Config phase2Config = Config(458, 72019 * 100);

int solvePhase1([Config config = phase1Config]) {
  Game game = Game(config);
  while (game.makeMove()) {
//    print(
//        'cur: ${game.curNode} (${game.curNode.previous}-> x <-${game.curNode.next})');
  }
  return game.getHighScore();
}

int solvePhase2() {
  Game game = Game(phase2Config);
  while (game.makeMove()) {}
  return game.getHighScore();
}

void main() {
  print('Phase 1: Ans = ${solvePhase1()}');
  print('Phase 2: Ans = ${solvePhase2()}');
}
