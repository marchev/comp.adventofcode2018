/// https://adventofcode.com/2018, Day 18
///
/// Task: https://adventofcode.com/2018/day/18
/// Date: 2018-12-25
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: 456225, for 0.02s
/// Phase 2 answer: 190164, for 0.4s

import 'dart:io';

List<String> readRows() {
  return File('lib/problem18/input.txt').readAsLinesSync().toList();
}

List<List<String>> createGrid(List<String> rows) {
  return rows.map((row) => row.split('')).toList();
}

void printGrid(List<List<String>> g) {
  g.forEach((row) {
    print(row.join(''));
  });
  print('');
}

const List<int> dx = [0, 1, 0, -1, 1, 1, -1, -1];
const List<int> dy = [1, 0, -1, 0, 1, -1, 1, -1];

List<List<String>> progress(List<List<String>> g) {
  List<List<String>> newG =
      List.generate(g.length, (_) => List.filled(g[0].length, ''));
  for (int y = 0; y < g.length; ++y) {
    for (int x = 0; x < g[y].length; ++x) {
      int numLumber = 0;
      int numTree = 0;
      for (int i = 0; i < 8; ++i) {
        int nx = x + dx[i];
        int ny = y + dy[i];
        if (0 <= nx && nx < g[y].length && 0 <= ny && ny < g.length) {
          numLumber += g[ny][nx] == '#' ? 1 : 0;
          numTree += g[ny][nx] == '|' ? 1 : 0;
        }
      }

      if (g[y][x] == '.') {
        newG[y][x] = numTree >= 3 ? '|' : g[y][x];
      } else if (g[y][x] == '|') {
        newG[y][x] = numLumber >= 3 ? '#' : g[y][x];
      } else {
        newG[y][x] = numLumber >= 1 && numTree >= 1 ? g[y][x] : '.';
      }
    }
  }
  return newG;
}

int countCellsOfType(List<List<String>> g, String type) => g
    .map((row) => row.where((cell) => cell == type).length)
    .fold(0, (a, b) => a + b);

int solvePhase1(List<List<String>> g) {
  for (int time = 0; time < 10; ++time) {
    g = progress(g);
    // printGrid(g);
  }
  return countCellsOfType(g, '#') * countCellsOfType(g, '|');
}

String serialize(List<List<String>> g) => g.map((row) => row.join()).join();

int solvePhase2(List<List<String>> g) {
  Map<String, int> gridToTimeSeen = {serialize(g): 0};
  int cycleStart, cycleLen;
  for (int time = 1;; ++time) {
    g = progress(g);
    String serialized = serialize(g);
    if (gridToTimeSeen.containsKey(serialized)) {
      cycleLen = time - gridToTimeSeen[serialized];
      cycleStart = gridToTimeSeen[serialized];
      break;
    }
    gridToTimeSeen[serialized] = time;
  }

  int numMoreIterationsNeeded = (1000000000 - cycleStart) % cycleLen;
  for (int i = 0; i < numMoreIterationsNeeded; ++i) {
    g = progress(g);
  }

  return countCellsOfType(g, '#') * countCellsOfType(g, '|');
}

void main() {
  List<List<String>> grid = createGrid(readRows());

  print('Phase 1: Ans = ${solvePhase1(grid)}');
  print('Phase 2: Ans = ${solvePhase2(grid)}');
}
