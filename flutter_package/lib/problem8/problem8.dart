/// https://adventofcode.com/2018, Day 8
///
/// Task: https://adventofcode.com/2018/day/8
/// Date: 2018-12-24
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: 43825, for 0.03s
/// Phase 2 answer: 19276, for 0.02s

import 'dart:io';

class Node {
  List<Node> kids = [];
  List<int> meta = [];
}

class Tree {
  Node root;

  Tree.from(List<int> serialized) {
    root = _readSubtree(serialized, [0]);
  }

  Node _readSubtree(List<int> serialized, List<int> i) {
    Node n = Node();
    int numKids = serialized[i[0]++];
    int numMeta = serialized[i[0]++];
    for (int k = 0; k < numKids; ++k) {
      n.kids.add(_readSubtree(serialized, i));
    }
    for (int k = 0; k < numMeta; ++k) {
      n.meta.add(serialized[i[0]++]);
    }
    return n;
  }
}

List<int> readTreeSerialization() {
  String line = File('lib/problem8/input.txt').readAsStringSync().trimRight();
  return line.split(' ').map((s) => int.parse(s)).toList();
}

int solvePhase1(List<int> serialization) {
  int sumMeta(Node n) {
    return n.meta.fold(0, (a, b) => a + b) +
        n.kids.map((kid) => sumMeta(kid)).fold(0, (a, b) => a + b);
  }

  Tree tree = Tree.from(serialization);
  return sumMeta(tree.root);
}

int solvePhase2(List<int> serialization) {
  int computeNode(Node n) {
    if (n.kids.length == 0) {
      return n.meta.fold(0, (a, b) => a + b);
    }
    return n.meta
        .where((index) => 1 <= index && index <= n.kids.length)
        .map((index) => computeNode(n.kids[index - 1]))
        .fold(0, (a, b) => a + b);
  }

  Tree tree = Tree.from(serialization);
  return computeNode(tree.root);
}

void main() {
  List<int> serialization = readTreeSerialization();

  print('Phase 1: Ans = ${solvePhase1(serialization)}');
  print('Phase 2: Ans = ${solvePhase2(serialization)}');
}
