/// https://adventofcode.com/2018, Day 14
///
/// Task: https://adventofcode.com/2018/day/14
/// Date: 2018-12-25
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: 4138145721, for 0.05s
/// Phase 2 answer: 20276284, for 1s

import 'dart:math';

String solvePhase1([int numRecipes = 793061]) {
  List<int> list = [3, 7];
  int p = 0, q = 1;
  while (list.length < numRecipes + 10) {
    int s = list[p] + list[q];
    if (s > 9) {
      list.add(1);
    }
    list.add(s % 10);

    p = (p + 1 + list[p]) % list.length;
    q = (q + 1 + list[q]) % list.length;
  }
  return list.skip(numRecipes).take(10).join();
}

int solvePhase2([String suffix = '793061']) {
  int suffixNum = int.parse(suffix);
  int suffixLen = suffix.length;
  int suffixPow10 = pow(10, suffixLen);

  List<int> list = [3, 7];
  int p = 0, q = 1;
  int curSuffix = 37;
  while (true) {
    int s = list[p] + list[q];

    if (s > 9) {
      list.add(1);
      curSuffix = (curSuffix * 10 + 1) % suffixPow10;
      if (curSuffix == suffixNum && list.length >= suffixLen) {
        return list.length - suffix.toString().length;
      }
    }

    list.add(s % 10);
    curSuffix = (curSuffix * 10 + s % 10) % suffixPow10;
    if (curSuffix == suffixNum && list.length >= suffixLen) {
      return list.length - suffix.toString().length;
    }

    p = (p + 1 + list[p]) % list.length;
    q = (q + 1 + list[q]) % list.length;
  }
}

void main() {
  print('Phase 1: Ans = ${solvePhase1()}');
  print('Phase 2: Ans = ${solvePhase2()}');
}
