/// https://adventofcode.com/2018, Day 23
///
/// Task: https://adventofcode.com/2018/day/23
/// Date: 2019-01-02
/// Author: Svilen Marchev
///
/// Status: works for phase 1 only
///
/// Phase 1 answer: 935
/// Phase 2 answer: ?

import 'dart:io';
import 'package:quiver/iterables.dart' as iter;

class Bot {
  List<int> c;
  int r;

  Bot(this.c, this.r);

  int distanceTo(Bot b) =>
      List.generate(3, (i) => (c[i] - b.c[i]).abs()).reduce((a, b) => a + b);

  @override
  String toString() => '[${c.join(',')}-$r]';
}

List<String> readLines() =>
    File('lib/problem23/input.txt').readAsLinesSync().toList();

List<Bot> convertToBots(List<String> lines) {
  RegExp pattern = RegExp(r'[^\d,-]');
  return lines.map((line) {
    var tokens =
        line.replaceAll(pattern, '').split(',').map(int.parse).toList();
    return Bot(tokens.sublist(0, 3), tokens[3]);
  }).toList();
}

int solvePhase1(List<Bot> bots) {
  Bot strongest = iter.max(bots, (b1, b2) => b1.r.compareTo(b2.r));
  return bots.where((bot) => bot.distanceTo(strongest) <= strongest.r).length;
}

void main() {
  List<Bot> bots = convertToBots(readLines());

  print('Phase 1: Ans = ${solvePhase1(bots)}');
}
