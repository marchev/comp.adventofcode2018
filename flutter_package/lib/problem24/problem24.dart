/// https://adventofcode.com/2018, Day 24
///
/// Task: https://adventofcode.com/2018/day/24
/// Date: 2019-01-02
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: 9878, for 0.08s
/// Phase 2 answer: 10954

import 'dart:math';
import 'package:quiver/iterables.dart' as iterables;

/// First list contains immune groups. Seconds one - infection groups.
/// The original list is patched with extra '()' whenever no immunities or
/// weaknesses are present.
List<List<String>> readInput() => List()
  ..add([
    '522 units each with 9327 hit points () with an attack that does 177 slashing damage at initiative 14',
    '2801 units each with 3302 hit points () with an attack that does 10 bludgeoning damage at initiative 7',
    '112 units each with 11322 hit points () with an attack that does 809 slashing damage at initiative 8',
    '2974 units each with 9012 hit points () with an attack that does 23 slashing damage at initiative 11',
    '4805 units each with 8717 hit points (weak to radiation) with an attack that does 15 bludgeoning damage at initiative 5',
    '1466 units each with 2562 hit points (immune to radiation, fire) with an attack that does 17 radiation damage at initiative 10',
    '2513 units each with 1251 hit points (immune to cold; weak to fire) with an attack that does 4 slashing damage at initiative 3',
    '6333 units each with 9557 hit points (immune to slashing) with an attack that does 14 fire damage at initiative 9',
    '2582 units each with 1539 hit points (immune to bludgeoning) with an attack that does 5 slashing damage at initiative 2',
    '2508 units each with 8154 hit points (weak to bludgeoning, cold) with an attack that does 27 bludgeoning damage at initiative 4',
  ])
  ..add([
    '2766 units each with 20953 hit points (weak to fire) with an attack that does 14 radiation damage at initiative 1',
    '4633 units each with 18565 hit points (immune to cold, slashing) with an attack that does 6 fire damage at initiative 15',
    '239 units each with 47909 hit points (weak to slashing, cold) with an attack that does 320 slashing damage at initiative 16',
    '409 units each with 50778 hit points (immune to radiation) with an attack that does 226 fire damage at initiative 17',
    '1280 units each with 54232 hit points (immune to slashing, fire, bludgeoning) with an attack that does 60 bludgeoning damage at initiative 13',
    '451 units each with 38251 hit points (immune to bludgeoning) with an attack that does 163 bludgeoning damage at initiative 6',
    '1987 units each with 37058 hit points () with an attack that does 31 slashing damage at initiative 20',
    '1183 units each with 19147 hit points (weak to slashing) with an attack that does 24 fire damage at initiative 12',
    '133 units each with 22945 hit points (weak to slashing; immune to cold, bludgeoning) with an attack that does 287 radiation damage at initiative 19',
    '908 units each with 47778 hit points () with an attack that does 97 fire damage at initiative 18',
  ]);

enum GroupType { immune, infection }

class Group {
  GroupType type;

  int numUnits;
  int hitPoints;
  List<String> weaknesses;
  List<String> immunities;
  String hitType;
  int initiative;
  int damage;

  Group(
      {this.type,
      this.numUnits,
      this.hitPoints,
      this.weaknesses,
      this.immunities,
      this.hitType,
      this.initiative,
      this.damage});

  int get power => numUnits * damage;

  @override
  String toString() =>
      '$type: units=$numUnits,hit=$hitPoints,weak=$weaknesses,immunes=$immunities,hitType=$hitType,initiative=$initiative,damage=$damage: power=$power';
}

class Game {
  List<Group> groups;

  Game(List<List<String>> lines, [int immuneDamageBoost = 0]) {
    groups = iterables.concat([
      lines[0].map((line) =>
          _parseGroup(line, GroupType.immune)..damage += immuneDamageBoost),
      lines[1].map((line) => _parseGroup(line, GroupType.infection)),
    ]).toList();
  }

  /// Simulates the whole game and returns the winner. null means the game has
  /// been a draw.
  GroupType complete() {
    while (true) {
      if (!_progress()) return null;
      if (getAliveGroups(GroupType.infection).length == 0 ||
          getAliveGroups(GroupType.immune).length == 0) {
        break;
      }
    }
    return getAliveGroups().first.type;
  }

  List<Group> getAliveGroups([GroupType type]) => groups
      .where((g) => g.numUnits > 0 && (type == null || g.type == type))
      .toList();

  /// Returns false if no progress was made, i.e. the game is a draw.
  bool _progress() {
    Map<Group, Group> attackerToTarget = _selectTargets();
    return _attack(attackerToTarget);
  }

  Map<Group, Group> _selectTargets() {
    Map<Group, Group> matching = Map();
    Set<Group> selectedTargets = Set();

    _createAttackSelectionOrder().forEach((attacker) {
      List<Group> candidates = getAliveGroups(_oppositeType(attacker.type))
          .where((g) => !selectedTargets.contains(g))
          .toList();
      if (candidates.isEmpty) return;

      Group target = iterables.min(candidates, (c1, c2) {
        int d1 = _computeDamage(attacker, c1);
        int d2 = _computeDamage(attacker, c2);
        if (d1 != d2) return -d1.compareTo(d2);
        if (c1.power != c2.power) return -c1.power.compareTo(c2.power);
        return -c1.initiative.compareTo(c2.initiative);
      });
      if (_computeDamage(attacker, target) > 0) {
        matching[attacker] = target;
        selectedTargets.add(target);
      }
    });

    return matching;
  }

  bool _attack(Map<Group, Group> attackerToTarget) {
    bool stateChanged = false;
    _createAttackOrder(attackerToTarget).forEach((attacker) {
      if (attacker.numUnits == 0) return;
      Group target = attackerToTarget[attacker];
      int damage = _computeDamage(attacker, target);
      int numDamaged = min(target.numUnits, damage ~/ target.hitPoints);

      stateChanged |= numDamaged > 0;
      target.numUnits -= numDamaged;
    });
    return stateChanged;
  }

  int _computeDamage(Group attacker, Group target) {
    if (target.immunities.contains(attacker.hitType)) return 0;
    if (target.weaknesses.contains(attacker.hitType)) return 2 * attacker.power;
    return attacker.power;
  }

  GroupType _oppositeType(GroupType type) =>
      type == GroupType.immune ? GroupType.infection : GroupType.immune;

  List<Group> _createAttackSelectionOrder() {
    return List.of(getAliveGroups())
      ..sort((g1, g2) {
        if (g1.power != g2.power) return -g1.power.compareTo(g2.power);
        return -g1.initiative.compareTo(g2.initiative);
      });
  }

  List<Group> _createAttackOrder(Map<Group, Group> attackerToTarget) {
    return List.of(attackerToTarget.keys)
      ..sort((a1, a2) => -a1.initiative.compareTo(a2.initiative));
  }

  Group _parseGroup(String line, GroupType type) {
    RegExp pattern = RegExp(
        r'^(\d+) units each with (\d+) hit points [(]([^)]*)[)] with an attack that does (\d+) (\w+) damage at initiative (\d+)');
    Match m = pattern.firstMatch(line);

    List<String> weaknesses = [], immunities = [];
    m.group(3).split('; ').forEach((section) {
      if (section.startsWith('weak to ')) {
        weaknesses = section.substring('weak to '.length).split(', ');
      } else if (section.startsWith('immune to ')) {
        immunities = section.substring('immune to '.length).split(', ');
      }
    });

    return Group(
      type: type,
      numUnits: int.parse(m.group(1)),
      hitPoints: int.parse(m.group(2)),
      weaknesses: weaknesses,
      immunities: immunities,
      damage: int.parse(m.group(4)),
      hitType: m.group(5),
      initiative: int.parse(m.group(6)),
    );
  }

  @override
  String toString() => groups.map((g) => g.toString()).join('\n') + '\n';
}

int solvePhase1(List<List<String>> lines) {
  Game game = Game(lines);
//  print(game);
  game.complete();
  return game.getAliveGroups().map((g) => g.numUnits).fold(0, (a, b) => a + b);
}

int solvePhase2(List<List<String>> lines) {
  // Assumes without any boost the infection wins (or it's a draw).
  int maxBoostForWhichInfectionWinsOrIsDraw = 0;
  for (int boost = 1 << 10; boost > 0; boost ~/= 2) {
    Game game = Game(lines, maxBoostForWhichInfectionWinsOrIsDraw + boost);
    if (game.complete() != GroupType.immune) {
      maxBoostForWhichInfectionWinsOrIsDraw += boost;
    }
  }

  Game game = Game(lines, maxBoostForWhichInfectionWinsOrIsDraw + 1);
  GroupType winner = game.complete();
  assert(winner == GroupType.immune);
  return game.getAliveGroups().map((g) => g.numUnits).fold(0, (a, b) => a + b);
}

void main() {
  List<List<String>> lines = readInput();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
