/// https://adventofcode.com/2018, Day 25
///
/// Task: https://adventofcode.com/2018/day/25
/// Date: 2019-01-02
/// Author: Svilen Marchev
///
/// Status: works for phase 1 only
///
/// Phase 1 answer: 420, for 0.1s
/// Phase 2 answer: ?

import 'dart:io';

class Point {
  List<int> c;

  Point(this.c);

  int distanceTo(Point p) =>
      List.generate(4, (i) => (c[i] - p.c[i]).abs()).reduce((a, b) => a + b);

  @override
  String toString() => '[${c.join(',')}]';
}

class UnionSets {
  List<int> parent;

  UnionSets(int size) {
    parent = List.generate(size, (i) => i);
  }

  void merge(int a, int b) {
    int rootA = _findRoot(a);
    int rootB = _findRoot(b);
    if (rootA != rootB) {
      parent[rootA] = rootB;
    }
  }

  int getNumSets() => parent.map(_findRoot).toSet().length;

  int _findRoot(int a) {
    int root;
    for (root = a; root != parent[root]; root = parent[root]) {}
    for (int n = a; n != root;) {
      int oldParent = parent[n];
      parent[n] = root;
      n = oldParent;
    }
    return root;
  }

  @override
  String toString() => '[${parent.join(',')}]';
}

List<String> readLines() =>
    File('lib/problem25/input.txt').readAsLinesSync().toList();

List<Point> convertToPoints(List<String> lines) => lines
    .map((line) => Point(line.split(',').map(int.parse).toList()))
    .toList();

int solvePhase1(List<Point> points) {
  UnionSets sets = UnionSets(points.length);
  for (int i = 0; i < points.length; ++i) {
    for (int j = i + 1; j < points.length; ++j) {
      if (points[i].distanceTo(points[j]) <= 3) {
        sets.merge(i, j);
//        print(sets);
      }
    }
  }
  return sets.getNumSets();
}

void main() {
  List<Point> points = convertToPoints(readLines());

  print('Phase 1: Ans = ${solvePhase1(points)}');
}
