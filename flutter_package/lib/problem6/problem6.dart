/// https://adventofcode.com/2018, Day 6
///
/// Task: https://adventofcode.com/2018/day/6
/// Date: 2018-12-24
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: 3890, for ~0.2s
/// Phase 2 answer: 40284, for ~0.1s

import 'dart:io';

import 'package:quiver/iterables.dart';

class Point {
  int r, c;

  Point(this.r, this.c);

  int distanceTo(Point p) => (r - p.r).abs() + (c - p.c).abs();

  @override
  String toString() => "($r,$c)";
}

class BoundingBox {
  Point tl, br;

  BoundingBox.of(List<Point> points) {
    tl = Point(min(points.map((p) => p.r)), min(points.map((p) => p.c)));
    br = Point(max(points.map((p) => p.r)), max(points.map((p) => p.c)));
  }

  @override
  String toString() => "[$tl, $br]";
}

List<Point> readPoints() {
  return File('lib/problem6/input.txt').readAsLinesSync().map((line) {
    var tokens = line.split(', ');
    return Point(int.parse(tokens[0]), int.parse(tokens[1]));
  }).toList();
}

/// Finds the closest point to a given base point. Returns null if the closest
/// point is not unique.
Point findClosestUniquePoint(Point base, List<Point> points) {
  List<int> distances =
      points.map((p) => p.distanceTo(base)).toList(growable: false);
  var minDist = min(distances);
  var index = distances.indexOf(minDist);
  if (index != distances.lastIndexOf(minDist)) {
    return null;
  }
  return points[index];
}

int solvePhase1(List<Point> points) {
  BoundingBox box = BoundingBox.of(points);
//  print('Bounding box: $box');

  Set<Point> pointsWithInfiniteArea = Set();
  for (int r = box.tl.r; r <= box.br.r; ++r) {
    Point closestTopLeft = findClosestUniquePoint(Point(r, box.tl.c), points);
    Point closestToRight = findClosestUniquePoint(Point(r, box.br.c), points);
    closestTopLeft != null && pointsWithInfiniteArea.add(closestTopLeft);
    closestToRight != null && pointsWithInfiniteArea.add(closestToRight);
  }
  for (int c = box.tl.c; c <= box.br.c; ++c) {
    Point closestToTop = findClosestUniquePoint(Point(box.tl.r, c), points);
    Point closestToBottom = findClosestUniquePoint(Point(box.br.r, c), points);
    closestToTop != null && pointsWithInfiniteArea.add(closestToTop);
    closestToBottom != null && pointsWithInfiniteArea.add(closestToBottom);
  }
//  print('pointsWithInfiniteArea: $pointsWithInfiniteArea');

  Map<Point, int> pointToArea = Map();
  for (int r = box.tl.r; r <= box.br.r; ++r) {
    for (int c = box.tl.c; c <= box.br.c; ++c) {
      Point closest = findClosestUniquePoint(Point(r, c), points);
      if (closest != null && !pointsWithInfiniteArea.contains(closest)) {
        pointToArea.update(closest, (val) => val + 1, ifAbsent: () => 1);
      }
    }
  }

  return max(pointToArea.values);
}

int solvePhase2(List<Point> points, {int maxDist = 10000}) {
  BoundingBox box = BoundingBox.of(points);
  int totalArea = 0;
  // Box should be extended so that all points from the area fit into it.
  // No need to extend it for the particular input of phase 2.
  int off = 0;
  for (int r = box.tl.r - off; r <= box.br.r + off; ++r) {
    for (int c = box.tl.c - off; c <= box.br.c + off; ++c) {
      Point base = Point(r, c);
      int totalDist =
          points.map((p) => p.distanceTo(base)).reduce((a, b) => a + b);
      totalArea += (totalDist < maxDist) ? 1 : 0;
    }
  }
  return totalArea;
}

void main() {
  List<Point> points = readPoints();

  print('Phase 1: Ans = ${solvePhase1(points)}');
  print('Phase 2: Ans = ${solvePhase2(points)}');
}
