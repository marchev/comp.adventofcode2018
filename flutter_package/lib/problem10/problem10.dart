/// https://adventofcode.com/2018, Day 10
///
/// Task: https://adventofcode.com/2018/day/10
/// Date: 2018-12-24
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: EJXNCCNX, for 1s
/// Phase 2 answer: 10612, for 1s

import 'dart:io';
import 'package:quiver/iterables.dart';

class Point {
  int r, c;

  Point(this.r, this.c);

  int distanceTo(Point p) => (r - p.r).abs() + (c - p.c).abs();

  @override
  String toString() => "($r,$c)";
}

class Beam {
  Point initialPos;

  /// Velocity vector
  int dr, dc;

  Beam(this.initialPos, this.dr, this.dc);

  @override
  String toString() => '[$initialPos @ $dr,$dc]';
}

class BoundingBox {
  Point tl, br;

  BoundingBox.of(Iterable<Point> points) {
    tl = Point(min(points.map((p) => p.r)), min(points.map((p) => p.c)));
    br = Point(max(points.map((p) => p.r)), max(points.map((p) => p.c)));
  }

  int get rows => br.r - tl.r + 1;

  int get cols => br.c - tl.c + 1;

  @override
  String toString() => "[$tl, $br]";
}

List<Beam> readBeams() {
  // Format is: position=< 10775, -31651> velocity=<-1,  3>
  RegExp pattern = RegExp(
      r'position=< ?([-\d]+),  ?([-\d]+)> velocity=< ?([-\d]+),  ?([-\d]+)>');
  return File('lib/problem10/input.txt').readAsLinesSync().map((line) {
    Match m = pattern.firstMatch(line);
    return Beam(Point(int.parse(m.group(2)), int.parse(m.group(1))),
        int.parse(m.group(4)), int.parse(m.group(3)));
  }).toList();
}

Set<Point> computePositionsAfterTime(List<Beam> beams, int time) {
  return Set.of(beams.map((beam) => Point(
      beam.initialPos.r + time * beam.dr, beam.initialPos.c + time * beam.dc)));
}

void printPoints(Iterable<Point> points) {
  BoundingBox box = BoundingBox.of(points);
  List<List<String>> a =
      List.generate(box.rows, (_) => List.filled(box.cols, '.'));
  points.forEach((p) {
    a[p.r - box.tl.r][p.c - box.tl.c] = '#';
  });
  a.forEach((row) {
    print(row.join());
  });
  print('');
}

/// Computes the time at which the snapshot of the beams would have
/// the smallest bounding box possible, and therefore would have the highest
/// chances to present a readable message.
int computeTime(List<Beam> beams) {
  int minBoxSeenAtTime = 0;
  int minBox;
  // Upper bound is experimentally found. An alternative would be to observe
  // the growth of the bounding box's area over time - once it starts growing,
  // it would be fine to stop.
  for (int time = 0; time < 11000; ++time) {
    if (time % 1000 == 0) print('time = $time');
    Iterable<Point> snapshot = computePositionsAfterTime(beams, time);
    // printPoints(snapshot);
    BoundingBox box = BoundingBox.of(snapshot);
    if (minBox == null || minBox > box.rows * box.cols) {
      minBox = box.rows * box.cols;
      minBoxSeenAtTime = time;
    }
  }
  return minBoxSeenAtTime;
}

void solvePhase1(List<Beam> beams) {
  printPoints(computePositionsAfterTime(beams, computeTime(beams)));
}

int solvePhase2(List<Beam> beams) {
  return computeTime(beams);
}

void main() {
  List<Beam> beams = readBeams();

  print('Phase 1: Ans:');
  solvePhase1(beams);
  print('Phase 2: Ans = ${solvePhase2(beams)}');
}
