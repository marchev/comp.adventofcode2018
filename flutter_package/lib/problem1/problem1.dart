// https://adventofcode.com/2018, Day 1
// Date: 2018-12-23
// Author: Svilen Marchev
// Status: works for both phases

import 'dart:io';

List<num> readFrequencyChanges() {
  return new File('lib/problem1/input.txt')
      .readAsLinesSync()
      .map((line) => int.parse(line))
      .toList();
}

num solvePart1(List<num> frequencyChanges) {
  var balance = 0;
  for (var change in frequencyChanges) {
    balance += change;
  }
  return balance;
}

num solvePart2(List<num> frequencyChanges) {
  Set<num> seen = Set();
  var balance = 0;
  for (var i = 0;; ++i) {
    if (!seen.add(balance)) {
      return balance;
    }
    balance += frequencyChanges[i % frequencyChanges.length];
  }
}

void main(List<String> args) {
  List<num> frequencyChanges = readFrequencyChanges();

  print("Part 1: Ans = ${solvePart1(frequencyChanges)}");
  print("Part 2: Ans = ${solvePart2(frequencyChanges)}");
}
