import 'dart:io';

List<String> readIds() {
  return File("lib/problem2/input.txt").readAsLinesSync();
}

num solvePhase1(List<String> ids) {
  Map<int, int> computeCharCodeToOccurMap(String s) {
    Map<int, int> map = Map();
    for (var c in s.codeUnits) {
      map.putIfAbsent(c, () => 0);
      map[c]++;
    }
    return map;
  }

  bool hasCharWithNumOccurs(Map<int, int> charCodeToNumOccurs, int numOccurs) {
    for (var value in charCodeToNumOccurs.values) {
      if (value == numOccurs) {
        return true;
      }
    }
    return false;
  }

  var num2 = 0;
  var num3 = 0;
  for (var id in ids) {
    Map<int, int> charCodeToNumOccurs = computeCharCodeToOccurMap(id);
    num2 += hasCharWithNumOccurs(charCodeToNumOccurs, 2) ? 1 : 0;
    num3 += hasCharWithNumOccurs(charCodeToNumOccurs, 3) ? 1 : 0;
  }
  return num2 * num3;
}

String solvePhase2(List<String> ids) {
  int computeStringDistance(String s1, String s2) {
    assert(s1.length == s2.length);
    int distance = 0;
    for (int i = 0; i < s1.length; ++i) {
      if (s1[i] != s2[i]) {
        distance++;
      }
    }
    return distance;
  }

  String removeDifferingChar(String s1, String s2) {
    StringBuffer sb = StringBuffer();
    for (int i = 0; i < s1.length; ++i) {
      if (s1[i] == s2[i]) {
        sb.write(s1[i]);
      }
    }
    return sb.toString();
  }

  for (int i = 0; i < ids.length; ++i) {
    for (int j = i + 1; j < ids.length; ++j) {
      if (computeStringDistance(ids[i], ids[j]) == 1) {
        return removeDifferingChar(ids[i], ids[j]);
      }
    }
  }
  throw AssertionError();
}

void main(List<String> args) {
  List<String> ids = readIds();

  print("Phase 1: Ans = ${solvePhase1(ids)}");
  print("Phase 2: Ans = ${solvePhase2(ids)}");
}
