/// https://adventofcode.com/2018, Day 7
///
/// Task: https://adventofcode.com/2018/day/7
/// Date: 2018-12-24
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: BFKEGNOVATIHXYZRMCJDLSUPWQ, for 0.02s
/// Phase 2 answer: 1020, for 0.02s

import 'dart:io';

import 'package:quiver/iterables.dart';

class Edge {
  String from, to;

  Edge(this.from, this.to);

  @override
  String toString() => "($from -> $to)";
}

class ComputationResult {
  String order;
  int time;

  ComputationResult(this.order, this.time);
}

class Worker {
  bool isWorking = false;
  int finishTime;
  String workingOnNode;
}

typedef int GetTimeForTask(String task);

int getTimeForTask(String node) {
  return 60 + (node.codeUnitAt(0) - 'A'.codeUnitAt(0) + 1);
}

List<Edge> readEdges() {
  return File('lib/problem7/input.txt').readAsLinesSync().map((line) {
    RegExp pattern =
        RegExp(r'^Step (\w+) must be finished before step (\w+) can begin.$');
    Match match = pattern.firstMatch(line);
    return Edge(match.group(1), match.group(2));
  }).toList(growable: false);
}

ComputationResult compute(
    List<Edge> edges, int numWorkers, GetTimeForTask getTimeForTask) {
  Map<String, List<String>> graph = Map();
  Map<String, int> nodeToNumIncomingEdges = Map();
  edges.forEach((edge) {
    nodeToNumIncomingEdges[edge.from] = 0;
    nodeToNumIncomingEdges[edge.to] = 0;
    graph[edge.from] = [];
    graph[edge.to] = [];
  });
  edges.forEach((edge) {
    nodeToNumIncomingEdges[edge.to]++;
    graph[edge.from].add(edge.to);
  });

  List<Worker> workers = List.generate(numWorkers, (_) => Worker());

  StringBuffer order = StringBuffer();
  int curTime = 0;
  while (nodeToNumIncomingEdges.isNotEmpty || workers.any((w) => w.isWorking)) {
    while (workers.any((w) => !w.isWorking) &&
        nodeToNumIncomingEdges.entries.any((entry) => entry.value == 0)) {
      String node = min(nodeToNumIncomingEdges.entries
          .where((entry) => entry.value == 0)
          .map((entry) => entry.key));
      nodeToNumIncomingEdges.remove(node);

      Worker worker = workers.where((w) => !w.isWorking).first;
      worker.isWorking = true;
      worker.workingOnNode = node;
      worker.finishTime = curTime + getTimeForTask(node);
    }

    List<Worker> working = workers.where((w) => w.isWorking).toList();
    working.sort((w1, w2) {
      if (w1.finishTime != w2.finishTime) {
        return w1.finishTime.compareTo(w2.finishTime);
      }
      return w1.workingOnNode.compareTo(w2.workingOnNode);
    });
    Worker finishingWorker = working.first;
    String finishingNode = finishingWorker.workingOnNode;

    order.write(finishingNode);
    curTime = finishingWorker.finishTime;

    finishingWorker.isWorking = false;

    graph[finishingNode].forEach((to) {
      nodeToNumIncomingEdges[to]--;
    });
  }
  return ComputationResult(order.toString(), curTime);
}

String solvePhase1(List<Edge> edges) {
  ComputationResult result = compute(edges, 1, (node) => 1);
  return result.order;
}

int solvePhase2(List<Edge> edges,
    {numWorkers = 5, GetTimeForTask getTimeForTask = getTimeForTask}) {
  ComputationResult result = compute(edges, numWorkers, getTimeForTask);
  return result.time;
}

void main() {
  List<Edge> edges = readEdges();

  print('Phase 1: Ans = ${solvePhase1(edges)}');
  print('Phase 2: Ans = ${solvePhase2(edges)}');
}
