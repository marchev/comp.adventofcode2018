/// https://adventofcode.com/2018, Day 12
///
/// Task: https://adventofcode.com/2018/day/12
/// Date: 2018-12-25
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: 3915, for 0.03s
/// Phase 2 answer: 4900000001793, for 0.005s

import 'dart:io';

class Config {
  String initial;
  Map<String, String> rules;

  Config(this.initial, this.rules);
}

Config readConfig() {
  List<String> lines = File('lib/problem12/input.txt').readAsLinesSync();
  return Config(
      lines[0].split(': ')[1],
      Map.fromEntries(lines.skip(2).map(
          (line) => MapEntry(line.split(' => ')[0], line.split(' => ')[1]))));
}

int compute(Config conf, int numSteps) {
  if (numSteps >= 100) {
    // After the ~100th step the pattern does no longer change - it only
    // shifts to the right by 1 position in each step.
    // This pattern works only for the concrete input - it wouldn't work
    // for arbitrary input.
    const String resultAfter100 =
        '....................................##.##.##.##....##.##.##.##.##.##.##.##.##....##.##.##.##.##.##.##.##.##....##.##.##....##.##.##.##.##.##....##.##.##.##.##.##.##.##....##.##.##.##.##.##.##.##.##.##';

    int sum = 0;
    int numMoreStepsNeeded = numSteps - 100;
    for (int i = 0; i < resultAfter100.length; ++i) {
      if (resultAfter100[i] == '#') {
        sum += i + numMoreStepsNeeded;
      }
    }
    return sum;
  }

  int off = 2 * numSteps;
  List<String> a = List.filled(2 * off + conf.initial.length, '.');
  for (int i = 0; i < conf.initial.length; ++i) {
    a[off + i] = conf.initial[i];
  }
  // print(a.join());

  for (int step = 0; step < numSteps; ++step) {
    List<String> an = List.filled(a.length, '.');
    for (int i = 2; i < a.length - 2; ++i) {
      String pattern = a.sublist(i - 2, i + 3).join();
      an[i] = conf.rules[pattern] ?? '.'; // fallback for the test
    }
    a = an;
    // print(a.join());
  }

  int sum = 0;
  for (int i = 0; i < a.length; ++i) {
    if (a[i] == '#') {
      sum += i - off;
    }
  }
  return sum;
}

int solvePhase1(Config conf) => compute(conf, 20);

int solvePhase2(Config conf) => compute(conf, 50000000000);

void main() {
  Config conf = readConfig();

  print('Phase 1: Ans = ${solvePhase1(conf)}');
  print('Phase 2: Ans = ${solvePhase2(conf)}');
}
