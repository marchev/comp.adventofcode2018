/// https://adventofcode.com/2018, Day 13
///
/// Task: https://adventofcode.com/2018/day/13
/// Date: 2019-01-01
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: 80,100, for 0.07s
/// Phase 2 answer: 16,99

import 'dart:io';

class PlayerState {
  int id;
  int x, y;
  int dir;
  int turn;

  PlayerState(this.id, this.x, this.y, this.dir, this.turn);

  @override
  String toString() => '($id: $y,$x: dir=$dir,turn=$turn)';
}

class State {
  List<PlayerState> players;

  State(this.players);

  void normalize() {
    players.sort((p, q) {
      if (p.y != q.y) {
        return p.y.compareTo(q.y);
      }
      return p.x.compareTo(q.x);
    });
  }

  @override
  String toString() => players.map((p) => p.toString()).join('\n') + '\n';
}

class Game {
  // Order is: top, right, bottom, left
  final List<int> dy = [-1, 0, 1, 0];
  final List<int> dx = [0, 1, 0, -1];

  State state;
  List<String> grid;
  int numInitialPlayers;

  List<List<int>> positionToPlayerId;
  Set<int> collidedPlayers = Set();

  Game(List<String> lines) {
    grid = lines
        .map((line) => line.replaceAll(RegExp(r'[<>v\^]'), '*'))
        .toList(growable: false);

    positionToPlayerId =
        List.generate(grid.length, (_) => List.filled(grid[0].length, -1));

    state = State(List());
    for (int y = 0; y < lines.length; ++y) {
      for (int x = 0; x < lines[y].length; ++x) {
        int d = _getPlayerDir(lines[y][x]);
        if (d >= 0) {
          var p = PlayerState(state.players.length, x, y, d, 0);
          state.players.add(p);
          positionToPlayerId[y][x] = p.id;
        }
      }
    }
    numInitialPlayers = state.players.length;
//    print(state);
  }

  String progress() {
    String firstCollision;

    List<PlayerState> newPlayers = state.players.map((p) {
      if (collidedPlayers.contains(p.id)) {
        return null;
      }

      PlayerState np = this._nextPlayerState(p);
      if (positionToPlayerId[np.y][np.x] != -1) {
        firstCollision = firstCollision ?? '${np.x},${np.y}';

        // Mark both collided players to be deleted.
        collidedPlayers.add(np.id);
        collidedPlayers.add(positionToPlayerId[np.y][np.x]);
        positionToPlayerId[np.y][np.x] = -1;
      } else {
        positionToPlayerId[np.y][np.x] = np.id;
      }

      positionToPlayerId[p.y][p.x] = -1;

      return np;
    }).toList();

    state = State(newPlayers
        .where((p) => p != null && !collidedPlayers.contains(p.id))
        .toList())
      ..normalize();
//    print(state);

    return firstCollision;
  }

  PlayerState _nextPlayerState(PlayerState cur) {
    PlayerState next = PlayerState(cur.id, cur.x, cur.y, cur.dir, cur.turn);

    if (grid[next.y][next.x] == '+') {
      // -1 means left of the cur one, 0 means straight, +1 means to the right
      int turnOffset = next.turn - 1;
      next.dir = (next.dir + turnOffset + 4) % 4;
      next.turn = (next.turn + 1) % 3;
      next.y = next.y + dy[next.dir];
      next.x = next.x + dx[next.dir];
      return next;
    }

    if (grid[next.y][next.x] == '/') {
      if (next.dir == 0 || next.dir == 2) {
        next.dir = (next.dir + 1) % 4;
      } else {
        next.dir = (next.dir + 3) % 4;
      }
    } else if (grid[next.y][next.x] == r'\') {
      if (next.dir == 0 || next.dir == 2) {
        next.dir = (next.dir + 3) % 4;
      } else {
        next.dir = (next.dir + 1) % 4;
      }
    }
    next.y = next.y + dy[next.dir];
    next.x = next.x + dx[next.dir];

    if (next.y < 0 ||
        next.y >= grid.length ||
        next.x < 0 ||
        next.x >= grid[0].length) {
      throw new AssertionError(
          'violation: ${next}, grid: ${grid[cur.y][cur.x]}');
    }

    return next;
  }

  int _getPlayerDir(String s) => '^>v<'.indexOf(s);
}

List<String> readLines() => File('lib/problem13/input.txt').readAsLinesSync();

String solvePhase1(List<String> lines) {
  Game game = Game(lines);
  while (true) {
    String collision = game.progress();
    if (collision != null) {
      return collision;
    }
  }
}

String solvePhase2(List<String> lines) {
  Game game = Game(lines);
  while (game.collidedPlayers.length < game.numInitialPlayers - 1) {
    game.progress();
  }
  PlayerState p = game.state.players.first;
  return '${p.x},${p.y}';
}

void main() {
  List<String> lines = readLines();

  print('Phase 1: Ans = ${solvePhase1(lines)}');
  print('Phase 2: Ans = ${solvePhase2(lines)}');
}
