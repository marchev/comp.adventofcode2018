/// https://adventofcode.com/2018, Day 17
///
/// Task: https://adventofcode.com/2018/day/17
/// Date: 2018-12-25
/// Author: Svilen Marchev
///
/// Status: works for both phases
///
/// Phase 1 answer: 31953, for 0.06s
/// Phase 2 answer: 26410, for 0.06s

import 'dart:io';
import 'package:quiver/iterables.dart';

class ClaySegment {
  int x1, x2;
  int y1, y2;

  ClaySegment(this.x1, this.x2, this.y1, this.y2);

  @override
  String toString() => x1 == x2 ? '($x2,$y1-$y2)' : '($x1-$x2,$y1)';
}

List<ClaySegment> readSegments() {
  // Format is: x=484, y=1286..1289
  RegExp yRangePattern = RegExp(r'x=(\d+), y=(\d+)\.\.(\d+)');
  // Format is: y=289, x=475..560
  RegExp xRangePattern = RegExp(r'y=(\d+), x=(\d+)\.\.(\d+)');
  return File('lib/problem17/input.txt').readAsLinesSync().map((line) {
    Match m = yRangePattern.firstMatch(line);
    if (m != null) {
      return ClaySegment(
        int.parse(m.group(1)),
        int.parse(m.group(1)),
        int.parse(m.group(2)),
        int.parse(m.group(3)),
      );
    }
    m = xRangePattern.firstMatch(line);
    return ClaySegment(
      int.parse(m.group(2)),
      int.parse(m.group(3)),
      int.parse(m.group(1)),
      int.parse(m.group(1)),
    );
  }).toList();
}

enum FillingType { finite, infinite }

const int sourceX = 500;

class Grid {
  List<ClaySegment> segments;
  List<List<String>> g;
  int minX; // all x-coordinates need to be translated by this value
  int minY, maxY;

  Grid(this.segments) {
    // Include the water source's x-coordinate (x=sourceX).
    // Add one more column to the left and to the right, which could be
    // touched by water.
    minX = min(segments.map((s) => s.x1).followedBy([sourceX])) - 1;
    int maxX = max(segments.map((s) => s.x2).followedBy([sourceX])) + 1;
    // Water source should not be included here.
    minY = min(segments.map((s) => s.y1));
    maxY = max(segments.map((s) => s.y2));

    g = List.generate(maxY + 1, (_) => List.filled(maxX - minX + 1, '.'));
    segments.forEach((s) {
      for (int y = s.y1; y <= s.y2; ++y) {
        for (int x = s.x1; x <= s.x2; ++x) {
          g[y][x - minX] = '#';
        }
      }
    });
  }

  void simulate() {
    _simulate(sourceX - minX, 0);
  }

  int countWateredCells() => _countCells('~|');

  int countAccumulatedCells() => _countCells('~');

  void printGrid() {
    g.forEach((line) {
      print(line.join());
    });
    print('');
  }

  FillingType _simulate(int x, int y) {
    if (y > maxY || g[y][x] == '|') {
      return FillingType.infinite;
    } else if (g[y][x] == '#' || g[y][x] == '~') {
      return FillingType.finite;
    }
    assert(g[y][x] == '.');

    FillingType curFillType = FillingType.finite;
    int leftX, rightX;

    for (leftX = x;; --leftX) {
      if (g[y][leftX] == '|') {
        curFillType = FillingType.infinite;
        break;
      } else if (g[y][leftX] == '#') {
        leftX++;
        break;
      } else {
        assert(g[y][leftX] == '.');
        FillingType bottomFillType = _simulate(leftX, y + 1);
        if (bottomFillType == FillingType.infinite) {
          curFillType = FillingType.infinite;
          break;
        }
      }
    }

    for (rightX = x;; ++rightX) {
      if (g[y][rightX] == '|') {
        curFillType = FillingType.infinite;
        break;
      } else if (g[y][rightX] == '#') {
        rightX--;
        break;
      } else {
        assert(g[y][rightX] == '.');
        FillingType bottomFillType = _simulate(rightX, y + 1);
        if (bottomFillType == FillingType.infinite) {
          curFillType = FillingType.infinite;
          break;
        }
      }
    }

    for (int k = leftX; k <= rightX; ++k) {
      g[y][k] = curFillType == FillingType.finite ? '~' : '|';
    }
    return curFillType;
  }

  int _countCells(String cellTypes) => g
      .skip(minY)
      .map((row) => row.where((cell) => cellTypes.contains(cell)).length)
      .fold(0, (a, b) => a + b);
}

int solvePhase1(List<ClaySegment> segments) {
  Grid grid = Grid(segments);
  grid.simulate();
  // grid.printGrid();
  return grid.countWateredCells();
}

int solvePhase2(List<ClaySegment> segments) {
  Grid grid = Grid(segments);
  grid.simulate();
  return grid.countAccumulatedCells();
}

void main() {
  List<ClaySegment> segments = readSegments();

  print('Phase 1: Ans = ${solvePhase1(segments)}');
  print('Phase 2: Ans = ${solvePhase2(segments)}');
}
