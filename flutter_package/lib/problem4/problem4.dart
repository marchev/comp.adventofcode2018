// https://adventofcode.com/2018, Day 4
//
// Task: https://adventofcode.com/2018/day/4
// Date: 2018-12-23
// Author: Svilen Marchev
// Status: works for both phases

import 'dart:io';
import 'package:quiver/iterables.dart';

enum EventType {
  startsShift,
  fallsAsleep,
  wakesUp,
}

class Event {
  DateTime time;
  EventType eventType;
  int guardId;

  Event(this.time, this.eventType, this.guardId);

  @override
  String toString() => "${this.time}: ${this.eventType} ${this.guardId}";
}

/// Parses entries following this format:
///
///  [1518-05-24 23:56] Guard #1721 begins shift
///  [1518-08-22 00:09] falls asleep
///  [1518-05-19 00:53] wakes up
///
/// and sorts them chronologically.
List<Event> readInput() {
  // Parses this format:
  // [...] ...
  RegExp timeRegEx = RegExp(r'^\[([^\]]+)\] (.+)$');
  // Parses this format:
  // Guard #1721 begins shift
  RegExp guardRegEx = RegExp(r'^Guard #(\d+) begins shift$');

  List<Event> events =
      File('lib/problem4/input.txt').readAsLinesSync().map((line) {
    Match timeMatch = timeRegEx.firstMatch(line);
    var timeString = timeMatch.group(1);
    var eventDesc = timeMatch.group(2);

    DateTime time = DateTime.parse(timeString);

    EventType eventType;
    int guardId;
    if (eventDesc == 'falls asleep') {
      eventType = EventType.fallsAsleep;
    } else if (eventDesc == 'wakes up') {
      eventType = EventType.wakesUp;
    } else {
      eventType = EventType.startsShift;
      Match guardMatch = guardRegEx.firstMatch(eventDesc);
      guardId = int.parse(guardMatch.group(1));
    }

    Event e = Event(time, eventType, guardId);
//    print("$e");
    return e;
  }).toList();
  events.sort((e1, e2) => e1.time.compareTo(e2.time));
  return events;
}

Map<int, List<int>> computeGuardToNumSleepsPerMinuteMap(List<Event> events) {
  Map<int, List<int>> guardToNumSleepsPerMinute = Map();

  int curGuardId;
  DateTime lastFallenAsleepTime;
  for (Event e in events) {
    if (e.eventType == EventType.startsShift) {
      curGuardId = e.guardId;
    } else if (e.eventType == EventType.fallsAsleep) {
      lastFallenAsleepTime = e.time;
    } else {
      // One counter for each minute in [00:00, 00:59].
      List<int> numSleepsPerMinute = guardToNumSleepsPerMinute.putIfAbsent(
          curGuardId, () => List.filled(60, 0));
      for (int t = lastFallenAsleepTime.minute; t < e.time.minute; ++t) {
        numSleepsPerMinute[t]++;
      }
    }
  }

  return guardToNumSleepsPerMinute;
}

num solvePhase1(List<Event> events) {
  Map<int, List<int>> guardToNumSleepsPerMinute =
      computeGuardToNumSleepsPerMinuteMap(events);

  Map<int, int> guardToTotalSleep = guardToNumSleepsPerMinute.map(
      (guardId, numSleepsPerMin) =>
          MapEntry(guardId, numSleepsPerMin.reduce((a, b) => a + b)));

  // Find the sleepiest guard.
  int sleepiestGuardId;
  for (MapEntry entry in guardToTotalSleep.entries) {
    sleepiestGuardId ??= entry.key;
    if (guardToTotalSleep[sleepiestGuardId] < entry.value) {
      sleepiestGuardId = entry.key;
    }
  }
  print('Sleepiest guard: $sleepiestGuardId');

  // Find the sleepiest guard's most popular sleeping minute.
  List<int> numSleepsPerMinute = guardToNumSleepsPerMinute[sleepiestGuardId];
  int sleepiestMinute = numSleepsPerMinute.indexOf(max(numSleepsPerMinute));
  print('Sleepiest minute: $sleepiestMinute');

  return sleepiestGuardId * sleepiestMinute;
}

num solvePhase2(List<Event> events) {
  Map<int, List<int>> guardToNumSleepsPerMinute =
      computeGuardToNumSleepsPerMinuteMap(events);

  int maxSleepsPerMin = max(guardToNumSleepsPerMinute.values
      .map((numSleepsPerMin) => max(numSleepsPerMin)));
  for (var entry in guardToNumSleepsPerMinute.entries) {
    var sleepiestMin = entry.value.indexOf(maxSleepsPerMin);
    if (sleepiestMin >= 0) {
      return entry.key * sleepiestMin;
    }
  }
  throw AssertionError();
}

void main() {
  List<Event> events = readInput();

  print('Phase 1: Ans = ${solvePhase1(events)}');
  print('Phase 2: Ans = ${solvePhase2(events)}');
}
